import time

from paho.mqtt.client import Client

from pahotoolkit import (
  start_async,
  stop_async,
  on_connect,
  subscribe,
)

from my_settings import (
  HOST,
  USERNAME,
  PASSWORD,
)


@subscribe('/temperature')
def handle_temperature(mqtt_client: Client, userdata, message):
    print(f'Got {message}')


@on_connect()
def client_connected(mqtt_client: Client, *args, **kwargs):
    """Publish something on_connect."""
    mqtt_client.publish('/clients', 'Paho\'s MQTT toolkit message!')


def main():
    # the default MQTT Client uses the default's MQTT Client constructor
    # https://github.com/eclipse/paho.mqtt.python#client-1
    start_async(host=HOST,  # port default to 1883
                username=USERNAME, password=PASSWORD)  # optional fields

    while True:
        time.sleep(1)  # or do something in this thread...


if __name__ == '__main__':
    try:
        main()
    finally:
        stop_async()
